---
title: Screenshots
heading: This is what using XMR.ID looks like.
---



### Monero CLI

    [wallet 48HKem]: transfer saberhagen.xmr.id 0.18081
    For URL: saberhagen.xmr.id, DNSSEC validation passed
     Monero Address = 874xgosgpSVJ1y5N8wybRGa5QrR3zKi8AWojr2pwnMB3YPNQTSPZMe9BhExgMbx2pQaYG6Yf8TJ6gJdRkQFSyouy4ZqWt6u
    Is this OK?  (Y/Yes/N/No): Yes
    Wallet password:
    
    Transaction 1/1:
    Spending from address index 0
    Sending 0.180810000000.  The transaction fee is 0.000019500000
    
    Is this okay?  (Y/Yes/N/No): Yes



### Monero GUI

![GUI XMR ID](/images/dark_gui_xmrid.gif)

{% comment %}
![GUI XMR ID entered](/images/gui_black_xmrid_entered.png)
![GUI XMR ID resolved](/images/gui_black_xmrid_resolved.png)

![GUI XMR ID entered](/images/gui_white_xmrid_entered.png)
![GUI XMR ID resolved](/images/gui_white_xmrid_resolved.png)
{% endcomment %}
